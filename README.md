# Install minikube

```bash
brew update
brew install minikube
minikube
minikube start
```

# Start pods

```bash
minikube start
kubectl apply -f mongo-secret.yaml
kubectl apply -f mongo-configmap.yaml
kubectl apply -f mongo.yaml
kubectl apply -f mongo-express.yaml
kubectl get all
kubectl get pod
kubectl describe pod <your-pod-id>
kubectl logs <yout-pod-id>
minikube service mongo-express-service
```
